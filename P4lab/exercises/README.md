# P4 Tutorial

This is the P4 exercises repo for the NetProg Course, Welcome!

The first 3 main exercises are taken from https://git.cse.kau.se/courses/dvad40/vt19/-/tree/master/P4lab/exercises 
and from the official P4 tutorial from https://p4.org.

The next one belongs to the Ulisse Research lab https://ulisse.unibo.it

## Official P4 exercises

1. Introduction and Language Basics
* [Basic Forwarding](./basic)
* [Basic Tunneling](./basic_tunnel)
* [Explicit Congestion Notification](./ecn)

2. Load Balancing
* [Load Balancing](./load_balance)
* [HULA](./hula)

3. In Network Monitoring, Control and Caching
* [MRI - Telemetry collection](./mri)

## Ulisse P4 exercises

4. Packet Mirroring

|   [**Simple network with ingress 2 egress cloning**](./clone_examples/basic/) |                         Description                          |  
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| <img src="misc/img/P4img-basic.png" alt="basic-i2e.png" style="zoom:30%;"/> | A 3 hosts, 1 switch network that clones every packet exchanged between h1 and h2 to h3. The clone happens between the ingress and egress queue.|
|   [**Simple network with egress 2 egress cloning**](./clone_examples/basic_e2e/) |                **Description**                                 |  
|   <img src="misc/img/P4img-basic.png" alt="basic-e2e.png" style="zoom:30%;"/> |A 3 hosts, 1 switch network that clones every packet exchanged between h1 and h2 to h3.The clone happens between the egress and egress queue.|
|   [**Ingress 2 egress cloning and packet tunneling**](./clone_examples/clone_with_tunnel/) |   **Description**                            |  
|   <img src="misc/img/P4img-basic_with_tunnel.png" alt="tunnel.png" style="zoom:50%;"/> |A 4 hosts, 2 switch network that clones every packet exchanged between h1 and h2 to h3 with the help of a tunneling rule. Packets exchanged between h3 and h4 are tunneled to h3 as well.|
|   [**Multiple cloning based on the source address**](./clone_examples/clone_multiple_mirroring/) |   **Description**                            |  
|   <img src="misc/img/P4img-multiple_cloning.png" alt="multiple_cloning.png" style="zoom:50%;"/> |A 6 hosts, 1 switch network that clones every packet exchanged coming from h1 to h4, from h2 to h5 and from h3 to h6.|
|   [**Programmable cloning**](./clone_examples/dynamic_fw) |   **Description**                            |  
|   <img src="misc/img/P4img-dynamic_fw.png" alt="programmable.png" style="zoom:50%;"/> |A 5 hosts, 1 switch network in which host1 sets which cloning channel is enabled at the moment.|

5. Asymmetric Flow Detection without window
* [Asymmetric Flow Detection with drop-only behavior and without window time](./5_asymmetric_flow)

6. Asymmetric Flow Detection with Window
* [Asymmetric Flow Detection with Windows Time](./6_asymmetrical_flow_windows-master)

5. Delay Link 
* [Delay Link Calculation](./7_P4_delayLink)


This repo would not have been possible without the help of:

* Amir Al Sadi @alsadiamir
* Fabio Merizzi @fmerizzi