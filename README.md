# NetProg_Course

Material for Network Programmability Course at University of Bologna.

This repository contain slides, exercises and an environment 
for a small course on Data Plane Programmability with P4 Language https://p4.org

Some material has been forked from two main repositories:

* Official P4 repo https://github.com/p4lang
* Data Plane Programming course from https://hhk3.kau.se/dpp/about-the-course-2/ 
and his repository https://git.cse.kau.se/courses/dvad40/vt19

We both thanks the owner of this material for their precious work.

Every other resources belong to the Ulisse Lab https://ulisse.unibo.it 
of University of Bologna